import numpy as np
from flask import Flask, render_template,request
import pickle

app = Flask(__name__)
model = pickle.load(open('model.pkl', 'rb'))

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/predict',methods=['POST'])
def predict():
    #For rendering results on HTML GUI
    int_features = [float(x) for x in request.form.values()]
    final_features = [np.array(int_features)]
    print(final_features)
    prediction = model.predict_proba(final_features)
    output = str(round(prediction[0][1], 2)*100)+'%'
    return render_template('index.html', prediction_text='Chance of developing heart disease is {}'.format(output))

if __name__ == "__main__":
    app.run(debug=True)